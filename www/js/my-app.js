// Initialize your app
var myApp = new Framework7();

// Export selectors engine
var $$ = Dom7;
//myApp.device.statusBar = false;
// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

$$(document).on('deviceready', function() {
    //StatusBar.overlaysWebView(true);
    //StatusBar.hide();
     window.open = cordova.InAppBrowser.open;
});


// Callbacks to run specific code for specific pages, for example for About page:
/*myApp.onPageInit('about', function (page) {
    // run createContentPage func after link was clicked
    $$('.create-page').on('click', function () {
        createContentPage();
    });
    
});*/

myApp.onPageInit('index', function (page) {

    // run createContentPage func after link was clicked
    

    var token = localStorage.getItem("tokenapp");

   

    if(token != null){
      interescauses();
    }else{
       mainView.router.loadPage({
            url: 'login.html',
            reload: true,
            animatePages: false,
        });
    }
    $('#cerrar').on('click', function() {
      localStorage.setItem("tokenapp", null);
      localStorage.setItem("user", null);
        mainView.router.loadPage({
            url: 'login.html',
            reload: true,
            animatePages: false,
        });
    });

    menutop();
    contenidoindex();

    
    


    
     
});
myApp.onPageInit('login', function (page) {

  $('#login').on('click', function() {
      var email = $('#email').val();
      var password = $('#pass').val();
      $.ajax({
        type  : "POST",
        dataType: 'json',
        cache : false,
        url   : "http://wehelp.trial-web.net/rest_api/authenticate.json",
        data  : {  login: email, password: password },
        error: function(data) {
          //console.log('error');
          myApp.addNotification({
                title: 'Error',
                message: 'Usuario o contraseña incorrectos'
            });
        },
        success: function(data) {

          if( data.user != "login error"){

            localStorage.setItem("tokenapp", data.user.User.token);

            localStorage.setItem("user", data.user.User.name);
            interescauses();
             mainView.router.loadPage({
                  url: 'index.html',
                  reload: true,
                  animatePages: false,
             });


          }else{

              myApp.addNotification({
                  title: 'Error',
                  message: 'Usuario o contraseña incorrectos'
              });
              localStorage.setItem("tokenapp", null);
              localStorage.setItem("user", null);
             
          }
          

          return true;
        },
      });
  });

   
     
});

myApp.onPageInit('register', function (page) {
    // run createContentPage func after link was clicked
   
    //$(".selectpicker").selectpicker(); 
    $.ajax({
      type  : "GET",
      dataType: 'json',
      cache : false,
      url   : "http://wehelp.trial-web.net/rest_api/countries.json",
      //data  : { Country: { id: id } },
      error: function(data) {
        //console.log('error');
      },
      success: function(data) {
        // rellenar Cenas
        
        $.each(data.countries, function(val, text) {
           /* myList.appendItem({
                title: text.Route.name,
                num: text.Route.village_count,
                id: text.Route.id,

             });*/
        $('#countries').append('<option value="'+text.State.id+'">'+text.State.name+'</option>');
        });

        return true;
      },
    });

    $('#volver').on('click', function() {
         mainView.router.loadPage({
            url: 'login.html',
            reload: true,
            animatePages: false,
        });
    });
     $('#continuar').on('click', function() {
        var genere = "";
        var name = $('#name').val();
        var surnames = $('#surname').val();
        genere = $('input[name=sex]:checked').val();
        //var genere = $('input:radio"').val();
        var birthday = $('#datebirth').val();
        var state_id = $('#countries').val();
        var email = $('#mail').val();
        var pass = $('#passw').val();
        var rpass = $('#rpass').val();

        $.ajax({
            type  : "POST",
            dataType: 'json',
            cache : false,
            url   : "http://wehelp.trial-web.net/rest_api/register.json",
            data  : { User: {
              name:name, 
              surnames:surnames, 
              genere:genere,
              birthday:birthday,
              state_id:state_id,
              email:email,
              pass:pass,
              rpass:rpass,
            } },
            
            error: function(data) {
             
              
              myApp.addNotification({
                title: 'Error',
                message: data.error,
                message: 'Error inesperado, vuelva a intentarlo'
              });
         
            },
            success: function(data) {
              console.log(data);
              if(data.error){
                var message = '';
                if(data.error == 'name'){
                  message = 'Campo Nombre no puede estar vacío';
                }else if(data.error == 'surnames'){
                  message = 'Campo de Apellidos no puede estar vacío';
                }else if(data.error == 'birthday'){
                  message = 'Campo Fecha nacimiento no puede estar vacío';
                }else if(data.error == 'email'){
                  message = 'Campo Email no puede estar vacío';
                }else if(data.error == 'state_id'){
                  message = 'Campo provincia no puede estar vacío';
                }else if(data.error == 'pass'){
                  message = 'Campos de Contraseñas no pueden estar vacios, y deben concidir';
                }else if(data.error == 'save'){
                  message = 'Error inesperado al registrase, vuelva a intentarlo';
                }
                myApp.addNotification({
                  title: 'Error',
                  message: message
                });
              }else{
                login(email, pass);
              }
              
              
           
            
           },
      });
        

     });
});

myApp.onPageInit('profile', function (page) {
    // run createContentPage func after link was clicked
   var token = localStorage.getItem("tokenapp");

      
        
    //$(".selectpicker").selectpicker(); 
    $.ajax({
      type  : "GET",
      dataType: 'json',
      cache : false,
      url   : "http://wehelp.trial-web.net/rest_api/countries.json",

      //data  : { Country: { id: id } },
      error: function(data) {
        //console.log('error');
      },
      success: function(data) {
        // rellenar Cenas
        
        $.each(data.countries, function(val, text) {
           /* myList.appendItem({
                title: text.Route.name,
                num: text.Route.village_count,
                id: text.Route.id,

             });*/
        $('#countries').append('<option value="'+text.State.id+'">'+text.State.name+'</option>');
        });

        return true;
      },
    });
    $.ajax({
      type  : "GET",
      dataType: 'json',
      cache : false,
      url   : "http://wehelp.trial-web.net/rest_api/viewprofile.json",
      headers: {
          "token":token
        },
      //data  : { Country: { id: id } },
      error: function(data) {
        //console.log('error');
      },
      success: function(data) {
        // rellenar Cenas
        
        $('#email').val(data.user.User.email);
        $('#name').val(data.user.User.name);
        $('#surname').val(data.user.User.surnames);
        $('#datebirth').val(data.user.User.birthdate);
        $('#countries').val(data.user.User.state_id);
 
       
        if(data.user.User.genere == 0){
          $('#m-option').prop('checked', true);
        }else{
          $('#f-option').prop('checked', true);
         
        }
        

        return true;
      },
    });

      $('#volver').on('click', function() {
        // mainView.router.back();
           mainView.router.back();
        
      });
     $('#continuar').on('click', function() {
        var genere = "";
        var name = $('#name').val();
        var surnames = $('#surname').val();
        genere = $('input[name=sex]:checked').val();
        //var genere = $('input:radio"').val();
        var birthday = $('#datebirth').val();
        var state_id = $('#countries').val();
        var email = $('#email').val();
        var pass = $('#pass').val();
        var rpass = $('#rpass').val();
   
        $.ajax({
            type  : "POST",
            dataType: 'json',
            cache : false,
            url   : "http://wehelp.trial-web.net/rest_api/editprofile.json",
            headers: {
              "token":token
            },
            data  : { User: {
              name:name, 
              surnames:surnames, 
              genere:genere,
              birthday:birthday,
              state_id:state_id,
              email:email,
              pass:pass,
              rpass:rpass,
            } },
            
            error: function(data) {
             
              
              myApp.addNotification({
                title: 'Error',
                message: data.error,
                message: 'Error inesperado, vuelva a intentarlo'
              });
         
            },
            success: function(data) {
              console.log(data);
              if(data.error){
                var message = '';
                if(data.error == 'name'){
                  message = 'Campo Nombre no puede estar vacío';
                }else if(data.error == 'surnames'){
                  message = 'Campo de Apellidos no puede estar vacío';
                }else if(data.error == 'birthday'){
                  message = 'Campo Fecha nacimiento no puede estar vacío';
                }else if(data.error == 'email'){
                  message = 'Campo Email no puede estar vacío';
                }else if(data.error == 'state_id'){
                  message = 'Campo provincia no puede estar vacío';
                }else if(data.error == 'pass'){
                  message = 'Campos de Contraseñas deben concidir';
                }else if(data.error == 'save'){
                  message = 'Error inesperado al registrase, vuelva a intentarlo';
                }
                myApp.addNotification({
                  title: 'Error',
                  message: message
                });
              }else{
                 myApp.addNotification({
                  title: 'Actualizado',
                  message: 'Tus datos han sido actualizados'
                });
                //login(email, pass);
              }
              
              
           
            
           },
      });
        

     });
});

myApp.onPageInit('tutorial', function (page) {
  $('.my-cardslider').cardslider({
      swipe: true,
      dots: true,
      loop: false,
      direction: 'left',
      nav: false,
  });
  $('#cerrar').on('click', function() {
      localStorage.setItem("tokenapp", null);
      localStorage.setItem("user", null);
      mainView.router.loadPage({
            url: 'login.html',
            reload: true,
            animatePages: false,
        });
  });
  menutop();
  
});
myApp.onPageInit('tutorial2', function (page) {
  $('.my-cardslider').cardslider({
      swipe: true,
      dots: true,
      loop: false,
      direction: 'right',
      nav: false,
  });
  
  
});
myApp.onPageInit('typeinterest', function (page) {
  $('.page-content').addClass('page-content-black');
  var contents = localStorage.getItem("contents");
  contents = JSON.parse(contents);
  $.each(contents, function(val, text) {
    $('.selectedcards').append('<div class="card"><input type="checkbox" name="type" value="'+text.Contenttype.id+'" ><a href="#"><img src="http://wehelp.trial-web.net/files/contenttype/icon/'+text.Contenttype.icon_dir+'/'+text.Contenttype.icon+'" alt="logo" class="logo"><span>'+text.Contenttype.name+'</span></a></div>');
  });
  $(".card").click(
    function () {
     
      if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $(this).find("input").attr('checked', true);
      }else{
        $(this).removeClass('active');
        $(this).find("input").attr('checked', false);
      }
    }
  );
    $('#anterior').click( function() {
        mainView.router.loadPage({
          url: 'tutorial.html',
          reload: true,
          animatePages: false,
        });
    });
    $('#siguiente').click( function() {

      var all = new Array();
      $("input:checkbox[name=type]:checked").each(function(){
          all.push($(this).val());
      });


      var token = localStorage.getItem("tokenapp");
   
      $.ajax({
        type  : "POST",
        dataType: 'json',
        cache : false,
        async : true,
        url   : "http://wehelp.trial-web.net/rest_api/addtypes/"+1+".json",
        data  : {  types: all },
        headers: {
          "token":token
        },
        error: function(data) {
          //console.log('error');
        
        },
        success: function(data) {
          
          mainView.router.loadPage({
            url: 'typecauses.html',
            reload: true,
            animatePages: false,
          });

          return true;
        },
      });

    });

});
myApp.onPageInit('typeinterest2', function (page) {
  $('.page-content').addClass('page-content-black');
  var contents = localStorage.getItem("contents");

  contents = JSON.parse(contents);
  

  $.each(contents, function(val, text) {
    $('.selectedcards').append('<div class="card"><input type="checkbox" name="type" value="'+text.Contenttype.id+'" ><a href="#"><img src="http://wehelp.trial-web.net/files/contenttype/icon/'+text.Contenttype.icon_dir+'/'+text.Contenttype.icon+'" alt="logo" class="logo"><span>'+text.Contenttype.name+'</span></a></div>');
  });
  $(".card").click(
    function () {
     
      if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $(this).find("input").attr('checked', true);
      }else{
        $(this).removeClass('active');
        $(this).find("input").attr('checked', false);
      }
    }
  );
    
    $('#siguiente').click( function() {

      var all = new Array();
      $("input:checkbox[name=type]:checked").each(function(){
          all.push($(this).val());
      });


      var token = localStorage.getItem("tokenapp");
   
      $.ajax({
        type  : "POST",
        dataType: 'json',
        cache : false,
        async : true,
        url   : "http://wehelp.trial-web.net/rest_api/addtypes/"+1+".json",
        data  : {  types: all },
        headers: {
          "token":token
        },
        error: function(data) {
          //console.log('error');
        
        },
        success: function(data) {
          /*$.ajax({
            type  : "GET",
            dataType: 'json',
            cache : false,
            async : true,
            url   : "http://wehelp.trial-web.net/rest_api/types/"+1+".json",
            //data  : {  login: email, password: password },
            error: function(data) {
              //console.log('error');
            },
            success: function(data) {
              localStorage.setItem("contents", JSON.stringify(data.types));
              

              return true;
            },
          });*/
          mainView.router.loadPage({
            url: 'index.html',
            reload: true,
            animatePages: false,
          });

          return true;
        },
      });

    });


  $.ajax({
      type  : "GET",
      dataType: 'json',
      cache : false,
      async : true,
      url   : "http://wehelp.trial-web.net/rest_api/viewtypes/"+1+".json",
      headers: {
          "token":token
      },
      //data  : {  login: email, password: password },
      error: function(data) {
        //console.log('error');
      },
      success: function(data) {

        //localStorage.setItem("contents", JSON.stringify(data.types));
        $.each(data.types, function(val, text) {
          var input = $('input[value="'+text.Contenttype.id+'"]').parent();
          if (!input.hasClass('active')) {
            input.addClass('active');
            input.find("input").attr('checked', true);
          }else{
            input.removeClass('active');
            input.find("input").attr('checked', false);
          }
          
          //$('.selectedcards').append('<div class="card"><input type="checkbox" name="type" value="'+text.Contenttype.id+'" ><a href="#"><img src="http://wehelp.trial-web.net/files/contenttype/icon/'+text.Contenttype.icon_dir+'/'+text.Contenttype.icon+'" alt="logo" class="logo"><span>'+text.Contenttype.name+'</span></a></div>');
        });

        return true;
      },
    });

    

});

myApp.onPageInit('typecauses', function (page) {
    $('.page-content').addClass('page-content-black');
    var causes = localStorage.getItem("causes");
    causes = JSON.parse(causes);
      $.each(causes, function(val, text) {
        $('.selectedcards').append('<div class="card"><input type="checkbox" name="type" value="'+text.Causestype.id+'" ><a href="#"><img src="http://wehelp.trial-web.net/files/causestype/icon/'+text.Causestype.icon_dir+'/'+text.Causestype.icon+'" alt="logo" class="logo"><span>'+text.Causestype.name+'</span></a></div>');
      });
      $(".card").click(
        function () {
         
          if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $(this).find("input").attr('checked', true);
          }else{
            $(this).removeClass('active');
            $(this).find("input").attr('checked', false);
          }
        }
      );
    $('#anterior').click( function() {
        mainView.router.loadPage({
          url: 'typeinterest.html',
          reload: true,
          animatePages: false,
        });
    });
    $('#siguiente').click( function() {
      var all = new Array();
      $("input:checkbox[name=type]:checked").each(function(){
          all.push($(this).val());
      });
      

      var token = localStorage.getItem("tokenapp");

      $.ajax({
        type  : "POST",
        dataType: 'json',
        cache : false,
        async : true,
        url   : "http://wehelp.trial-web.net/rest_api/addtypes/"+2+".json",
        data  : {  types: all },
        headers: {
          "token":token
        },
        error: function(data) {
          //console.log('error');
         
        },
        success: function(data) {
         
          mainView.router.loadPage({
            url: 'index.html',
            reload: true,
            animatePages: false,
          });

          return true;
        },
      });

    });

});

myApp.onPageInit('typecauses2', function (page) {
  $('.page-content').addClass('page-content-black');
 

  var contents = localStorage.getItem("causes");
  contents = JSON.parse(contents);
  

  $.each(contents, function(val, text) {
    $('.selectedcards').append('<div class="card"><input type="checkbox" name="type" value="'+text.Causestype.id+'" ><a href="#"><img src="http://wehelp.trial-web.net/files/causestype/icon/'+text.Causestype.icon_dir+'/'+text.Causestype.icon+'" alt="logo" class="logo"><span>'+text.Causestype.name+'</span></a></div>');
  });
  $(".card").click(
    function () {
     
      if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $(this).find("input").attr('checked', true);
      }else{
        $(this).removeClass('active');
        $(this).find("input").attr('checked', false);
      }
    }
  );

    $('#siguiente').click( function() {

      var all = new Array();
      $("input:checkbox[name=type]:checked").each(function(){
          all.push($(this).val());
      });


      var token = localStorage.getItem("tokenapp");
   
      $.ajax({
        type  : "POST",
        dataType: 'json',
        cache : false,
        async : true,
        url   : "http://wehelp.trial-web.net/rest_api/addtypes/"+2+".json",
        data  : {  types: all },
        headers: {
          "token":token
        },
        error: function(data) {
          //console.log('error');
        
        },
        success: function(data) {
          

          
          $.ajax({
            type  : "GET",
            dataType: 'json',
            cache : false,
            async : true,
            url   : "http://wehelp.trial-web.net/rest_api/types/"+2+".json",
            //data  : {  login: email, password: password },
            error: function(data) {
              //console.log('error');
            },
            success: function(data) {
              localStorage.setItem("causes", JSON.stringify(data.types));
              

              return true;
            },
          });

          mainView.router.loadPage({
            url: 'index.html',
            reload: true,
            animatePages: false,
          });

          return true;
        },
      });

    });

    $.ajax({
      type  : "GET",
      dataType: 'json',
      cache : false,
      async : true,
      url   : "http://wehelp.trial-web.net/rest_api/viewtypes/"+2+".json",
      headers: {
          "token":token
      },
      //data  : {  login: email, password: password },
      error: function(data) {
        //console.log('error');
      },
      success: function(data) {
        //localStorage.setItem("contents", JSON.stringify(data.types));
        $.each(data.types, function(val, text) {
          var input = $('input[value="'+text.Causestype.id+'"]').parent();
          if (!input.hasClass('active')) {
            input.addClass('active');
            input.find("input").attr('checked', true);
          }else{
            input.removeClass('active');
            input.find("input").attr('checked', false);
          }
          
          //$('.selectedcards').append('<div class="card"><input type="checkbox" name="type" value="'+text.Contenttype.id+'" ><a href="#"><img src="http://wehelp.trial-web.net/files/contenttype/icon/'+text.Contenttype.icon_dir+'/'+text.Contenttype.icon+'" alt="logo" class="logo"><span>'+text.Contenttype.name+'</span></a></div>');
        });

        return true;
      },
    });

});

myApp.onPageInit('about', function (page) {
  $('.backmenu').click( function() {

     mainView.router.loadPage({
        url: 'index.html',
        reload: true,
        animatePages: false,
      });
    
    // mainView.router.back();
     //mainView.router.back({ ignoreCache: true});

     //mainView.router.refreshPreviousPage();
  });  

});
myApp.onPageInit('stats', function (page) {
 
  $('#invertir').hide();
  $('#go').hide();

  var jelps = localStorage.getItem("jelps");
  var totaljelps = localStorage.getItem("totaljelps");
  if(jelps >= 50){
    $('#invertir').show();
  }else{
    $('#go').show();
  }
  $('.backmenu').click( function() {

     mainView.router.loadPage({
        url: 'index.html',
        reload: true,
        animatePages: false,
      });
    
    // mainView.router.back();
     //mainView.router.back({ ignoreCache: true});

     //mainView.router.refreshPreviousPage();
  });  

});

myApp.onPageInit('faq', function (page) {
  
  $.ajax({
        type  : "GET",
        dataType: 'json',
        cache : false,
        url   : "http://wehelp.trial-web.net/rest_api/faqs.json",
        error: function(data) {
          //console.log('error');
         
        },
        success: function(data) {
        
          $.each(data.faqs, function(val, text) {
           /* myList.appendItem({
                title: text.Route.name,
                num: text.Route.village_count,
                id: text.Route.id,

             });*/
            $('.textnormal').append('<div><span>'+text.Faq.question+'</span>'+text.Faq.answer+'</div>');
           
          });

          return true;
        },
      });

  $('.backmenu').click( function() {

     mainView.router.loadPage({
        url: 'index.html',
        reload: true,
        animatePages: false,
      });

    });



      
    
    // mainView.router.back();
     //mainView.router.back({ ignoreCache: true});

     //mainView.router.refreshPreviousPage();
}); 


myApp.onPageInit('causes', function (page) {
  var token = localStorage.getItem("tokenapp");
  var mycauses = localStorage.getItem('mycauses');
  mycauses = JSON.parse(mycauses);

  $.each(mycauses, function(val, text) {

    var txt = text.Cause.description;
    var texto = recortar(txt, 100);
    var texto2 = recortar2(txt, 100);
    if(text.Cause.wonjelps == null){
      text.Cause.wonjelps = 0;
    }
    var precent = (100*text.Cause.wonjelps)/text.Cause.jelps;

   // fechas
    var currentDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    var day = currentDate.getDate();
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear();
    //10/09/2014
    var hoy = day + "/" + month + "/" + year;

    var maxDate = text.Cause.date_end.split(' ');
    var datDate = maxDate[0].split('-');
    var manana = datDate['2']+'/'+datDate['1']+'/'+datDate['0'];
    var dias = restaFechas(hoy,manana);


    $('.cards ul').append('<li id="'+text.Mycause.id+'"><div class="cardo"><div class="screenclose"><a href="#" class="closescreen"><img src="img/cross-gray.png" alt=""> Cerrar</a></div><div class="imagenbg" style="background-image: url(http://wehelp.trial-web.net/files/cause/image/'+text.Cause.image_dir+'/'+text.Cause.image+');"></div><div class="scroll"><h2><img src="http://wehelp.trial-web.net/files/ong/image/'+text.Cause.Ong.image_dir+'/'+text.Cause.Ong.image+'" alt=""><span>'+text.Cause.Ong.name+'</span><a href="#" id="link'+text.Mycause.id+'">'+text.Cause.Ong.textlink+'</a></h2><div class="meta"><div class="metaleft">'+precent+'% de '+text.Cause.jelps +' Jelps</div><div class="metaright">'+dias+' días</div><div class="metacount"><span></span></div><b>'+text.Cause.wonjelps+' jelps</b></div><h3>'+text.Cause.name+'</h3>'+texto+'<div class="todotexto">'+texto2+'</div>                 <a href="#" class="buttomgreen ayuda" id="ayuda'+text.Mycause.id+'">Ayudar</a></div> </div></li>');

    $('#'+text.Mycause.id).find('.metacount span').css({'width':precent+'%'});
    $('#link'+text.Mycause.id).on('click', function() {
      var ref = window.open(text.Cause.Ong.link, '_blank', 'location=yes');
    });
    $('#ayuda'+text.Mycause.id).on('click', function() {
        addjelps(text.Mycause.id);
    });
   
       
});

  var cardslider =  $('.cards').cardslider({
      swipe: false,
      dots: true,
      loop: true,
      direction: 'left',
      nav: false,
  }).data('cardslider');
  menutop();
  $(".cards li").swipe( {
    swipeStatus:function(event, phase, direction, distance, duration, fingers, fingerData, currentDirection)
          {
       
          //leer más
          $('.reedmore').click( function() {
              $(this).parent().parent().parent().parent().find('.cardo').removeClass('downspam');
              $(this).parent().parent().parent().parent().find('.cardo').removeClass('rotatleft');
              $(this).parent().parent().parent().parent().find('.cardo').removeClass('rotatbackleft');
              $(this).parent().parent().parent().parent().find('.cardo').removeClass('rotatright');
              $(this).parent().parent().parent().parent().find('.cardo').removeClass('rotatbackright');
              $('.cards .cardslider__cards').addClass('screenfull');
              $('.cards').css({'margin-top': '0px'});
              $('.page-content').css({'padding-bottom': '0px'});
              $('.screenclose').show();
              $(".screenfull li .cardo").swipe( { allowPageScroll:"horizontal" } );
              $(this).parent().parent().find('.todotexto').show();
              $(this).hide();

          });
          $('.closescreen').click( function() {
              $('.cards .cardslider__cards').removeClass('screenfull');
              $(this).parent().parent().find('.todotexto').hide();
              $(this).parent().parent().find('.reedmore').show();
              $('.screenclose').hide();
              $('.cards').css({'margin-top': '20px'});
              $('.page-content').css({'padding-bottom': '44px'});
              
          });

          


          
            //izquierda
            if(direction == 'left' && phase == "end"){
                cardslider.nextCard();
                  
            //derecha
            }else if(direction == 'right' && phase == "end"){
              cardslider.prevCard();      
            }


          
           
             
          },
          

          
      });

}); 
myApp.onPageInit('contenidos', function (page) {
  
  var token = localStorage.getItem("tokenapp");


  /*var contents = localStorage.getItem("contents");
  contents = JSON.parse(contents);*/
 
          var mycontents = localStorage.getItem('mycontents');
          mycontents = JSON.parse(mycontents);
       
          $.each(mycontents, function(val, text) {

            var txt = text.Content.description;
            var texto = recortar(txt, 100);
            var texto2 = recortar2(txt, 100);
            var link = text.Content.link;
            var textlink = text.Content.textink;
            if(text.Content.link){
              var desliza = '<div class="swipup" alt="'+text.Content.link+'"><img src="img/shape.png" alt="swip">desliza hacia arriba y '+textlink+'</div>';
              var buttom = '<div class="vuttinup" id="vuttinup'+text.Mycontent.id+'">'+textlink+'</div>';
            }else{
              var desliza = '';
              var buttom = '';
            }
            if(text.Content.image){
              $('.cards ul').append('<li id="'+text.Mycontent.id+'"><span class="backcard"><img src="img/cross.png" alt="" class="cross"><img src="img/check.png" alt="" class="check"></span><div class="cardo"><div class="imagenbg" style="background-image: url(http://wehelp.trial-web.net/files/content/image/'+text.Content.image_dir+'/'+text.Content.image+');"></div><div class="scroll"><h2><img src="http://wehelp.trial-web.net/files/brand/image/'+text.Content.Brand.image_dir+'/'+text.Content.Brand.image+'" alt=""><span>'+text.Content.name+'</span></h2>'+texto+'<div class="todotexto">'+texto2+' '+buttom+'</div></div>'+desliza+'</div></li>');
            }else if(text.Content.url){
              $('.cards ul').append('<li id="'+text.Mycontent.id+'"><span class="backcard"><img src="img/cross.png" alt="" class="cross"><img src="img/check.png" alt="" class="check"></span><div class="cardo"><video poster="img/card.jpeg" id="bgvid" playsinline webkit-playsinline autoplay muted loop><source src="https://s3-eu-west-1.amazonaws.com/jewelp/'+text.Content.url+'" webkit-playsinline playsinline></video><img src="img/mute.png" alt="mute" id="muted"><div class="scroll"><h2><img src="http://wehelp.trial-web.net/files/brand/image/'+text.Content.Brand.image_dir+'/'+text.Content.Brand.image+'" alt=""><span>'+text.Content.name+'</span></h2>'+texto+'<div class="todotexto">'+texto2+' '+buttom+'</div></div>'+desliza+'</div></li>');
            }
            if(text.Content.link){
              $('#vuttinup'+text.Mycontent.id).on('click', function() {

                //var ref = cordova.InAppBrowser.open(text.Content.link ,'_blank', 'location=yes');
                //window.open = cordova.InAppBrowser.open;
                var ref = window.open(text.Content.link, '_blank', 'location=yes');
              });
              //$('.cards ul').append(html);
            }
               
        });
        uli = 1;
        timmer();
        rotatecards();
         $('video').prop('muted', true);
         $(".cardo video").click( function (){
          if( $(this).prop('muted') ) {
                $(this).prop('muted', false);
                $(this).parent().find('#muted').hide();
          } else {
            $(this).prop('muted', true);
            $(this).parent().find('#muted').show();
          }
        });
   
});  
myApp.onPageInit('endcontent', function (page) {
  
  var token = localStorage.getItem("tokenapp");
  uli = 1;                     //  set your counter to 1
  //$(".counter span").removeClass( 'terminate' );

  var cardslider =  $('.statscontent').cardslider({
      swipe: false,
      dots: false,
      loop: false,
      direction: 'left',
      nav: false,
  }).data('cardslider');

  var mycontents = localStorage.getItem('mycontents');
  mycontents = JSON.parse(mycontents);
  var all = new Array();
  var ids = new Array();
  var jelps = localStorage.getItem("jelps");
  jelps = parseInt(jelps) + 10;
  var totaljelps = localStorage.getItem("totaljelps");
  totaljelps = parseInt(totaljelps) + 10;

  $('.statscontent h5').html(jelps+' de 50 Jelps');
  $('#wejelpstotal').html(totaljelps+'Jelps');
  $.each(mycontents, function(val, text) { 
    
      all.push(text.Mycontent.id);//añadir id
      
      var ide = 0;
      $.each(ids, function(val, te) { 
    
        if(te == text.Content.Brand.id){
          ide = 1;
        }  
      });
      ids.push(text.Content.Brand.id);
      if(ide == 0){
          $('.logos').append('<span><img src="http://wehelp.trial-web.net/files/brand/image/'+text.Content.Brand.image_dir+'/'+text.Content.Brand.image+'" alt=""></span>');
      }
      

  });


  //add
      $.ajax({
        type  : "POST",
        dataType: 'json',
        cache : false,
        async : true,
        url   : "http://wehelp.trial-web.net/rest_api/contentviewcontent.json",
        data  : {  mycontents: all },
        headers: {
          "token":token
        },
        error: function(data) {
          //console.log('error');
        
        },
        success: function(data) {
           $.ajax({
              type  : "GET",
              dataType: 'json',
              cache : false,
              async : true,
              url   : "http://wehelp.trial-web.net/rest_api/viewprofile.json",
              headers: {
                  "token":token
                },
              //data  : { Country: { id: id } },
              error: function(data) {
                //console.log('error');
              },
              success: function(data) {
                // rellenar Cenas
                
                $('#email').val(data.user.User.email);
                localStorage.setItem("jelps", data.user.User.wejelps);
                localStorage.setItem("totaljelps", data.user.User.totalwejelps);

                return true;
              },
            });

        return true;
        },
      });

 


  

   
});



function login(email, password){
  
  $.ajax({
      type  : "POST",
      dataType: 'json',
      cache : false,
      url   : "http://wehelp.trial-web.net/rest_api/authenticate.json",
      data  : {  login: email, password: password },
      error: function(data) {
        //console.log('error');
        localStorage.setItem("tokenapp", null);
        localStorage.setItem("user", null);
      },
      success: function(data) {

        if( data.user != "login error"){

          localStorage.setItem("tokenapp", data.user.User.token);

          localStorage.setItem("user", data.user.User.name);
          localStorage.setItem("jelps", data.user.User.wejelps);
          localStorage.setItem("totaljelps", data.user.User.totalwejelps);
           mainView.router.loadPage({
                url: 'tutorial.html',
                reload: true,
                animatePages: false,
           });

        }else{

            /*myApp.addNotification({
                title: jqtranslate.get('Login'),
                message: jqtranslate.get('Login Error, try again!')
            });*/
            localStorage.setItem("tokenapp", null);
             localStorage.setItem("user", null);
           
        }
        

        return true;
      },
    });
}

function interescauses(){
  // causes
  $.ajax({
      type  : "GET",
      dataType: 'json',
      cache : false,
      async : true,
      url   : "http://wehelp.trial-web.net/rest_api/types/"+2+".json",
      //data  : {  login: email, password: password },
      error: function(data) {
        //console.log('error');
      },
      success: function(data) {
        localStorage.setItem("causes", JSON.stringify(data.types));
        

        return true;
      },
    });

  $.ajax({
      type  : "GET",
      dataType: 'json',
      cache : false,
      async : true,
      url   : "http://wehelp.trial-web.net/rest_api/types/"+1+".json",
      //data  : {  login: email, password: password },
      error: function(data) {
        //console.log('error');
      },
      success: function(data) {
        localStorage.setItem("contents", JSON.stringify(data.types));
        

        return true;
      },
    });

}
//  set your counter to 1
var uli = 1;                     
function timmer () {           //  create a loop function
   setTimeout(function () {    //  call a 3s setTimeout when the loop is called
      var percent = uli*3.333;
      if(uli == 30){
        percent = 100;
        $(".counter span").addClass( 'terminate' ); 
      }
      percent = Math.round(percent);
      $(".counter span").width( percent+'%' );  
      uli++;                     //  increment the counter
      if (uli <= 30) {            //  if the counter < 10, call the loop function
         timmer();             //  ..  again which will trigger another 
      }                        //  ..  setTimeout()
   }, 1000)
}

function recortar(cadena, num){

    var numero = num;
    var texto = cadena;
    var length = texto.length;
    var eltexto = texto.substr(0, num);
    //var arregloDeSubCadenas = texto.split(" ", numero);

    if( eltexto.length < length){
      eltexto = eltexto+' '+'<a href="#" class="reedmore">leer más</a>';
    }
   
   return eltexto;
}
function recortar2(cadena,num){
  
    var numero = num;
    var texto = cadena;
    var length = texto.length;
    var eltexto = texto.substr(num, length);
    //var arregloDeSubCadenas = texto.split(" ", numero);
    


   return eltexto;
}

function rotatecards(){
  var cardslider =  $('.cards').cardslider({
      swipe: false,
      dots: false,
      loop: false,
      direction: 'left',
      nav: false,
  }).data('cardslider');



  $(".cards li").swipe( {
      //$("video").prop('muted', false);
        //Generic swipe handler for all directions
        
        swipeStatus:function(event, phase, direction, distance, duration, fingers, fingerData, currentDirection)
        {
          // abajo

          
         
         if(direction == 'down'){
          if($('.cardslider__cards').hasClass('screenfull')){

            $('.cards .cardslider__cards').removeClass('screenfull');
            $(this).find('.todotexto').hide();
            $(this).find('.reedmore').show();
          }else{
            if(phase=="end"){

              
                $(this).find('.backcard').removeClass('red');
                $(this).find('.backcard').removeClass('green');
                // quitar transiciones ejcutadas derecha izq
                if($(this).find('.cardo').hasClass('rotatright')){
                    $(this).find('.cardo').addClass('rotatbackright');
                    $(this).find('.cardo').removeClass('rotatright');
                    $(this).find('.cardo').removeClass('rotatbackright');
                }
                if($(this).find('.cardo').hasClass('rotatleft')){
                  $(this).find('.cardo').addClass('rotatbackleft');
                  $(this).find('.cardo').removeClass('rotatleft');
                  $(this).find('.cardo').removeClass('rotatbackleft');
                }
                if($(this).find('.cardo').hasClass('upspam')){
                  $(this).find('.cardo').removeClass('upspam');
                }
                 $(this).find('.cardo').addClass('downspam');
                 $('#spam').show(); 

                 //
                 $(this).find('.backcard').css("background-color", "#767676");
                 $(this).find('.cross').hide();
                 $(this).find('.check').hide();
               
              }

            


          }
         }
         if(direction == 'up'){

          if($(this).find('.cardo').hasClass('downspam')){
              $(this).find('.cardo').addClass('upspam');
              $(this).find('.cardo').removeClass('downspam');
              $('#spam').hide();
               
          }else{
            if(phase=="end"){
              /*$(this).find('.cardo').removeClass('downspam');
              $(this).find('.cardo').removeClass('rotatleft');
              $(this).find('.cardo').removeClass('rotatbackleft');
              $(this).find('.cardo').removeClass('rotatright');
              $(this).find('.cardo').removeClass('rotatbackright');
              $('.cards .cardslider__cards').addClass('screenfull');
              $(".screenfull li .cardo").swipe( { allowPageScroll:"horizontal" } );*/
              var url = $(this).find('.swipup').attr('alt');
              /*var ref = cordova.InAppBrowser.open(url,'_blank', 'location=yes');
              window.open = cordova.InAppBrowser.open;*/

              var ref = window.open(url, '_blank', 'location=yes');
            }
              /*$('.screenfull li .cardo').scroll();
              $(".screenfull li .cardo").animate({
                scrollTop: distance
              }, 1);*/

              
              /*$('#div').scroll();
              $("#div").animate({
                scrollTop: 1000
              }, 2000);*/
          }

         
           
         }
         var id = $(this).attr('id');
         // eliminar tarjeta
        $('#spam').click( function() {
            
           $(".cards li").find('.downspam').parent().remove();
            $(this).hide();
            spam(id); 

            cardslider.destroy();
            var cardslider =  $('.cards').cardslider({
                 swipe: false,
                dots: false,
                loop: false,
                direction: 'left',
                nav: false,
            }).data('cardslider');

       
            
            

        });
        //leer más
        $('.reedmore').click( function() {

           
            $(this).parent().parent().parent().parent().find('.cardo').removeClass('downspam');
            $(this).parent().parent().parent().parent().find('.cardo').removeClass('rotatleft');
            $(this).parent().parent().parent().parent().find('.cardo').removeClass('rotatbackleft');
            $(this).parent().parent().parent().parent().find('.cardo').removeClass('rotatright');
            $(this).parent().parent().parent().parent().find('.cardo').removeClass('rotatbackright');
            $('.cards .cardslider__cards').addClass('screenfull');
            $(".screenfull li .cardo").swipe( { allowPageScroll:"horizontal" } );
            $(this).parent().parent().find('.todotexto').show();
            $(this).hide();

        });

        


        if($('.cardslider__cards').hasClass('screenfull')){

        }else{
          //izquierda
          if(direction == 'left'){

            if(duration > 1){
              $(this).find('.backcard').removeClass('red');
              $(this).find('.backcard').addClass('green');
              if($(this).find('.cardo').hasClass('rotatright')){
                $(this).find('.cardo').addClass('rotatbackright');
                $(this).find('.cardo').removeClass('rotatright');
                $(this).find('.cardo').removeClass('rotatbackright');
              }
              $(this).find('.cardo').addClass('rotatleft');
            }
            if (phase == "end"){
              
                if($('.counter span').hasClass('terminate')){
                    mainView.router.loadPage({
                          url: 'endcontent.html',
                          reload: true,
                          animatePages: false,
                     });

                }
                $(this).find('.backcard').removeClass('red');
                $(this).find('.backcard').removeClass('green');
                if($(this).find('.cardo').hasClass('rotatright')){
                    $(this).find('.cardo').addClass('rotatbackright');
                    $(this).find('.cardo').removeClass('rotatright');
                    $(this).find('.cardo').removeClass('rotatbackright');
                  }
                  if($(this).find('.cardo').hasClass('rotatleft')){
                    $(this).find('.cardo').addClass('rotatbackleft');
                    $(this).find('.cardo').removeClass('rotatleft');
                    $(this).find('.cardo').removeClass('rotatbackleft');
                  }
                $(this).find('.backcard').removeClass('red');
                $(this).find('.backcard').addClass('green');
                $(this).find('.cardo').addClass('rotatleft');  
                cardslider.nextCard();

                $(this).find('.backcard').delay( 2000 ).removeClass('red');
                $(this).find('.backcard').delay( 2000 ).removeClass('green');

                /*$(this).find('.left').delay( 2000 ).hide();
                $(this).find('.right').delay( 2000 ).hide();*/
                $(this).find('.cardo').delay( 2000 ).addClass('rotatbackleft');
                
            }
            
          //derecha
          }else if(direction == 'right'){
            if(duration > 1){
              $(this).find('.backcard').addClass('red');
              $(this).find('.backcard').removeClass('green');

              if($(this).find('.cardo').hasClass('rotatleft')){
                $(this).find('.cardo').addClass('rotatbackleft');
                $(this).find('.cardo').removeClass('rotatleft');
                $(this).find('.cardo').removeClass('rotatbackleft');
              }
              
              $(this).find('.cardo').addClass('rotatright');
            }
            if (phase=="end"){
                if($('.counter span').hasClass('terminate')){
                  mainView.router.loadPage({
                        url: 'endcontent.html',
                        reload: true,
                        animatePages: false,
                   });

                }
                $(this).find('.backcard').removeClass('red');
                $(this).find('.backcard').removeClass('green');
                if($(this).find('.cardo').hasClass('rotatleft')){
                  $(this).find('.cardo').addClass('rotatbackleft');
                  $(this).find('.cardo').removeClass('rotatleft');
                  $(this).find('.cardo').removeClass('rotatbackleft');
                }
                if($(this).find('.cardo').hasClass('rotatright')){
                  $(this).find('.cardo').addClass('rotatbackright');
                  $(this).find('.cardo').removeClass('rotatright');
                  $(this).find('.cardo').removeClass('rotatbackright');
                }
                //$(this).find('.cardo').addClass('cardslider--sortback-right');
                $(this).find('.backcard').addClass('red');
                $(this).find('.backcard').removeClass('green');
                $(this).find('.cardo').addClass('rotatright');
                cardslider.nextCard();
                $(this).find('.backcard').delay( 2000 ).removeClass('red');
                $(this).find('.backcard').delay( 2000 ).removeClass('green');
                $(this).find('.cardo').delay( 2000 ).addClass('rotatbackright');
                var id = $(this).attr('id');
                dontlike(id);
            }


            
          }
        }

         
           
        },
        

        
      });

}

function spam(id){
  var token = localStorage.getItem("tokenapp");
  $.ajax({
      type  : "GET",
      dataType: 'json',
      cache : false,
      async : true,
      url   : "http://wehelp.trial-web.net/rest_api/spamcontent/"+id+".json",
       headers: {
          "token":token
        },
      //data  : {  login: email, password: password },
      error: function(data) {
        //console.log('error');
      },
      success: function(data) {
        
        

        return true;
      },
    });

}


   
   


function dontlike(id){
  var token = localStorage.getItem("tokenapp");
  $.ajax({
      type  : "GET",
      dataType: 'json',
      cache : false,
      async : true,
      url   : "http://wehelp.trial-web.net/rest_api/dontlike/"+id+".json",
       headers: {
          "token":token
        },
      //data  : {  login: email, password: password },
      error: function(data) {
        console.log('error');
      },
      success: function(data) {
        
        

        return true;
      },
    });
}
function addjelps(id){
  var token = localStorage.getItem("tokenapp");
  $.ajax({
      type  : "GET",
      dataType: 'json',
      cache : false,
      async : true,
      url   : "http://wehelp.trial-web.net/rest_api/addjelpcause/"+id+".json",
       headers: {
          "token":token
        },
      //data  : {  login: email, password: password },
      error: function(data) {
        console.log('error');
      },
      success: function(data) {
        //mainView.router.load({pageName: 'index'});
        mainView.router.loadPage({
            url: 'index.html',
            reload: true,
            animatePages: false,
            force: true,
        });

        return true;
      },
    });
}

restaFechas = function(f1,f2)
 {
 var aFecha1 = f1.split('/'); 
 var aFecha2 = f2.split('/'); 
 var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
 var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
 var dif = fFecha2 - fFecha1;
 var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
 return dias;
 }
   


   

  


